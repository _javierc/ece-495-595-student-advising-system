class Appointment < ActiveRecord::Base
  belongs_to :advisor, :class_name => 'User', :foreign_key => 'advisor_id'
  belongs_to :student, :class_name => 'User', :foreign_key => 'student_id'
  
  belongs_to :note

  after_save :send_mail
  before_save :update_time_check_avail
  
  validates :advisor_id, presence: true
  validates :student_id, presence: true


  
  private
  def send_mail
    # only send an email if the start time has changed
    if start_time_changed?    
      UserMailer.appointment_notification_email(self.student, self).deliver_now
      UserMailer.appointment_notification_email_to_advisor(self.advisor, self).deliver_now
    end
    
    return true
  end

  # Queries the appointments to make sure time slot is avail
  #
  # Returns a list of times in the form of Time objects (for now).
  #
  def update_time_check_avail
    users_apps = Appointment.where(:advisor_id => self.advisor.id).order("end_time DESC")
    
    latest = users_apps.first
    
    if !latest.nil? && latest.end_time > self.start_time
      errors[:base] << "There is already an appointment scheduled at/during this time."
      return false
    end
    
    self.end_time = self.start_time + 60*30

    # automatically create a note
    if self.note.nil?
      _note = Note.new
      _note.save
      self.note = _note
    end
    
    
    return true
  end
  
end
