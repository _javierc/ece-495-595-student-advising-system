class User < ActiveRecord::Base
  
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :cas_authenticatable, :trackable
  has_and_belongs_to_many :roles
  belongs_to :department

  # TODO support for dual majors
  belongs_to :major
  
  has_many :appointments_advisor,
           :class_name => 'Appointment',
           :foreign_key => 'student_id'
  
  has_many :appointments_student,
           :class_name => 'Appointment',
           :foreign_key => 'advisor_id'

  
  validates :username, presence: true
  validates :email, presence: true
  validates :role_ids, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true

  has_many :courses,
           :class_name => "DegreeProgression",
           :foreign_key => "user_id"

  before_save :check_student_major

  def self.get_advisors
    role = Role.find_by(:name =>"Advisor")
    # User.where(role_id: role.id).all
    User.joins(:roles).where("roles_users.role_id is #{role.id}")
  end
  
  
  private
  def check_student_major
    if self.roles.exists?(:name => "Student") && self.major.nil?
      _major = Major.find_by(:name => "Undeclared")
      self.major = _major
    end
  end
end
