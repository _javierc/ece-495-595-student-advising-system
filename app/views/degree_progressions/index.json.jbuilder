json.array!(@degree_progressions) do |degree_progression|
  json.extract! degree_progression, :id, :user_id, :course_id, :is_complete
  json.url degree_progression_url(degree_progression, format: :json)
end
