Rails.application.routes.draw do
  
  resources :majors
  resources :courses
  resources :departments
  resources :colleges
  get 'pages/index'

  
  devise_for :users

  
  resources :appointments do
    resources :notes
  end

  resources :users#, :except => [:create, :new]

  resources :users do
    resources :appointments do    
      collection do
        get 'agenda'
      end
    end
    resources :degree_progressions
    member do
      get 'edit_student', :to => "users#edit_student"
    end
  end
  
  root to: "pages#index"

  # need to remove this
  resources :notes
  resources :degree_progressions
  
end
