require 'test_helper'

class DegreeProgressionsControllerTest < ActionController::TestCase
  setup do
    @degree_progression = degree_progressions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:degree_progressions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create degree_progression" do
    assert_difference('DegreeProgression.count') do
      post :create, degree_progression: { course_id: @degree_progression.course_id, is_complete: @degree_progression.is_complete, user_id: @degree_progression.user_id }
    end

    assert_redirected_to degree_progression_path(assigns(:degree_progression))
  end

  test "should show degree_progression" do
    get :show, id: @degree_progression
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @degree_progression
    assert_response :success
  end

  test "should update degree_progression" do
    patch :update, id: @degree_progression, degree_progression: { course_id: @degree_progression.course_id, is_complete: @degree_progression.is_complete, user_id: @degree_progression.user_id }
    assert_redirected_to degree_progression_path(assigns(:degree_progression))
  end

  test "should destroy degree_progression" do
    assert_difference('DegreeProgression.count', -1) do
      delete :destroy, id: @degree_progression
    end

    assert_redirected_to degree_progressions_path
  end
end
