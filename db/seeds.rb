# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Role.create :name => "Advisor"
Role.create :name => "Student"
Role.create :name => "Admin"


c = College.create(:name => "University College", :code => "UC")
d = Department.create(:name => "University College", :code => "UCD", :college => c)

# User.create(:first_name => "Susie",
#             :last_name => "Doe",
#             :email => "susie@unm.edu",
#             :username => "susie",
#             :department => d)


# create single major
Major.create(:name => "Liberal Arts", :department => d)
Major.create(:name => "Undeclared", :department => d)

c = College.create(:name => "School of Engineering", :code => "SOE")
d = Department.create(:name => "Computer Science", :code => "CS", :college => c)
# create cs major
Major.create(:name => "Computer Science", :department => d)

ece = Department.create(:name => "Electrical & Computer Engineering", :code => "ECE", :college => c)
Major.create(:name => "Computer Engineering", :department => ece)
Major.create(:name => "Electrical Engineering", :department => ece)


Course.create(:title => "152L: Computer Programming Fundamentals", :department => d)
Course.create(:title => "CS 251L: Intermediate Programming", :department => d)
Course.create(:title => "CS 261: Mathematical Foundations of Computer Science", :department => d)
Course.create(:title => "CS 241L: Data Organization", :department => d)
Course.create(:title => "CS 241L: Data Organization", :department => d)
Course.create(:title => "CS 351L: Design of Large Programs", :department => d)
Course.create(:title => "CS 375: Introduction to Numerical Computing", :department => d)
Course.create(:title => "CS 361L: Data Structures and Algorithms ", :department => d)
Course.create(:title => "CS 357L: Declarative Programming", :department => d)
Course.create(:title => "CS 362L: Data Structures and Algorithms II", :department => d)


